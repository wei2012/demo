package com.example.demo3.common;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadTest extends Thread {

    public ThreadTest(String name){
        super(name);
    }

    public ThreadTest(){
        super();
    }

    @Override
    public void run(){
        while (true){
            log.info("thread running ....");
        }

//        while (!interrupted()){
//            log.info(getName()+"线程执行了。。。");
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

    }
}
