package com.example.demo3.common;

import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorTest implements Runnable {

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println(Thread.currentThread().getName() + "start");
        long start = System.currentTimeMillis();
        int i = (int)(Math.random() * 1000);
        try {
            TimeUnit.MILLISECONDS.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "end" + "用时：" + (end - start));
    }

}

