package com.example.demo3.common;

public class ThreadVolatile {

    public static volatile boolean running = true;

    public static class AnotherThread extends Thread {

        @Override
        public void run() {
            System.out.println("AnotherThread is running");

            while (running) { }

            System.out.println("AnotherThread is stoped");
        }
    }

    public static void main(String[] args) throws Exception {
        new AnotherThread ().start();

        Thread.sleep(1000);
        // 1 秒之后想停止 AnotherThread
        running = false;
    }

}
