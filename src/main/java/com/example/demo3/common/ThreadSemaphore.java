package com.example.demo3.common;

import java.util.concurrent.Semaphore;

public class ThreadSemaphore extends Thread{
    //信号量
    private volatile Semaphore sem;

    //申请信号量的大小
    private int count;

    public ThreadSemaphore(Semaphore sem, int count) {
        this.sem = sem;
        this.count = count;
    }

    @Override
    public void run() {
        try {
            //System.out.println("============================start");
            // 从信号量中获取count个许可
            sem.acquire(count);
            //System.out.println("============================start2");
            //Thread.sleep(3);
            //System.out.println("============================start3");
            System.out.println(Thread.currentThread().getName() + " acquire count="+count);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("============================error");
        }catch (Exception e) {
            e.printStackTrace();
            System.err.println("============================error");
        } finally {
            try{
                // 释放给定数目的许可，将其返回到信号量。
                sem.release(count);
                System.out.println(Thread.currentThread().getName() + " release " + count + "");
            }
            catch (Exception e){
                e.printStackTrace();
                System.err.println(e.getMessage());
                System.err.println("============================error");
            }
        }
        //System.out.println("============================start4");
    }

}
