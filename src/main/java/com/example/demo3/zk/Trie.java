package com.example.demo3.zk;

public class Trie {
    private TrieNode root;
    public Trie(){
        root=new TrieNode();
    }

    /**
     * 插入
     * @param word
     */
    public void insert(String word){
        TrieNode node=root;
        for(int i=0;i<word.length();i++){
            char currentChar=word.charAt(i);
            if(!node.containsKey(currentChar)){
                node.put(currentChar,new TrieNode());
            }
            node=node.get(currentChar);
        }
        node.setEnd();
    }

    /**
     * 查找前缀
     * @param word
     * @return
     */
    private TrieNode searchPrefix(String word){
        TrieNode node=root;
        for(int i=0;i<word.length();i++){
            char curLetter=word.charAt(i);
            if(node.containsKey(curLetter)){
                node=node.get(curLetter);
            }
            else{
                return null;
            }
        }
        return node;
    }

    /**
     * 查找是否存在trie中间。
     * @param word
     * @return
     */
    public boolean search(String word){
        TrieNode node=searchPrefix(word);
        return node!=null && node.isEnd();
    }



}
