package com.example.demo3.zk;

/**
 * Key对象封装了index、status、交易次数count，作为map的key
 */
public class Key {
    private final int index;
    private final int status;
    private final int count;
    public Key(int index,int status,int count) {
        this.index = index;
        this.status = status;
        this.count = count;
    }
    //这里需要实现自定义的equals和hashCode函数
    @Override
    public int hashCode() {
        return this.index + this.status + this.count;
    }
    public boolean equals(Object obj) {
        Key other = (Key)obj;
        if(index==other.index && status==other.status && count==other.count) {
            return true;
        }
        return false;
    }
}
