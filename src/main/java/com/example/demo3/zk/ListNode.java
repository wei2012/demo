package com.example.demo3.zk;

/**
 * @author xiaozw
 */
public class ListNode {
    public int val;
    public ListNode next;
    public ListNode(int x) {
        val = x;
        next = null;
    }
}
