package com.example.demo3.zk;

import com.example.demo3.model.TreeNode;
import com.sun.deploy.panel.ITreeNode;

public class BinaryTree {
    private TreeNode ans;

    public BinaryTree() {
        this.ans = null;
    }

    /**
     * 二叉树的最近公共祖先
     * @param root
     * @param p
     * @param q
     * @return
     */
    public boolean dfs(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return false;
        }
        boolean lson = dfs(root.left, p, q);
        boolean rson = dfs(root.right, p, q);
        if ((lson && rson) || ((root.val == p.val || root.val == q.val) && (lson || rson))) {
            ans = root;
        }
        return lson || rson || (root.val == p.val || root.val == q.val);
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q){
        dfs(root,p,q);
        return this.ans;
    }

}
