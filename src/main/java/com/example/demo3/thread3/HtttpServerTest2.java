package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

@Slf4j
public class HtttpServerTest2 {
    public static void main(String[] args) throws Exception {
        log.info("启动服务器监听8888端口。。。");
        ServerSocket server=new ServerSocket(8888);
        while (!Thread.interrupted()){
            log.info("不停接受客户端请求。。。");
            Socket client= server.accept();
            log.info("获取输入输出流。。。");

            new Thread(new ServerThreadTest(client)).start();

        }
        server.close();
    }
}
