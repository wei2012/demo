package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

@Slf4j
public class HtttpServerTest {
    public static void main(String[] args) throws Exception {
        log.info("启动服务器监听8888端口。。。");
        ServerSocket server=new ServerSocket(8888);
        while (!Thread.interrupted()){
            log.info("不停接受客户端请求。。。");
            Socket client= server.accept();
            log.info("获取输入输出流。。。");
            InputStream ins=client.getInputStream();
            OutputStream out=client.getOutputStream();

            log.info("读取请求内容。。。");
//            int len=0;
//            byte[] b=new byte[1024];
//            len=ins.read(b);
//            log.info(new String(b,0,len));

//            while ((len=ins.read(b))!=-1){
//                log.info("返回："+new String(b,0,len));
//            }

            BufferedReader  reader=new BufferedReader(new InputStreamReader(ins));
            String line= reader.readLine();
            log.info("返回："+line);

            log.info("给用户响应。。。");
            PrintWriter pw =new PrintWriter(out);
            InputStream i =new FileInputStream("D:\\work\\temp\\index.html");
            BufferedReader fr=new BufferedReader(new InputStreamReader(i));

            pw.println("HTTP/1.1 200 OK");
            pw.println("Content-Type:text/html;charset=utf-8");
            pw.println("Content-Length:"+i.available());
            pw.println("Server:Hello");
            pw.println("Date:"+new Date());
            pw.println("");
            pw.flush();
            String c=null;
            while ((c=fr.readLine())!=null){
                pw.print(c);
            }
            pw.flush();
            fr.close();
            pw.close();
            reader.close();
            client.close();

        }
    }
}
