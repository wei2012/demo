package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.util.Date;

@Slf4j
public class ServerThreadTest implements Runnable {

    private Socket client;
    private InputStream ins;
    private OutputStream out;

    private PrintWriter pw;
    private BufferedReader br;

    public ServerThreadTest(Socket client) throws Exception {
        this.client = client;
        init();
    }

    private void init() throws Exception {
        log.info("获取输入输出流。。。");
        ins=client.getInputStream();
        out=client.getOutputStream();
    }

    @Override
    public void run()  {
        try {
            go();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void go() throws Exception {
        log.info("读取请求内容。。。");
//            int len=0;
//            byte[] b=new byte[1024];
//            len=ins.read(b);
//            log.info(new String(b,0,len));

//            while ((len=ins.read(b))!=-1){
//                log.info("返回："+new String(b,0,len));
//            }

        BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
        String line = reader.readLine();
        log.info("返回：" + line);

        log.info("给用户响应。。。");
        pw = new PrintWriter(out);
        InputStream i = new FileInputStream("D:\\work\\temp\\index.html");
        br = new BufferedReader(new InputStreamReader(i));

        pw.println("HTTP/1.1 200 OK");
        pw.println("Content-Type:text/html;charset=utf-8");
        pw.println("Content-Length:" + i.available());
        pw.println("Server:Hello");
        pw.println("Date:" + new Date());
        pw.println("");
        pw.flush();
        String c = null;
        while ((c = br.readLine()) != null) {
            pw.print(c);
        }
        pw.flush();
        br.close();
        pw.close();
        reader.close();
        client.close();
    }
}
