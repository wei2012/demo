package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.StampedLock;

@Slf4j
public class LongAddrTest {

    private int balance;

    private StampedLock lock=new StampedLock();

    public void optimisticRead(){
        long stamp= lock.tryOptimisticRead();
        int c=balance;
        log.info("可能出现写操作。。。");
        if(!lock.validate(stamp)){
            log.info("要重新读");
            long readStamp=lock.readLock();
            c =balance;
            stamp=readStamp;
        }
        log.info("释放锁。。。");
        lock.unlockRead(stamp);
    }

    public void read(){
        long stamp= lock.readLock();

        int c=balance;
        log.info("做一些操作。。。");
        lock.unlockRead(stamp);
    }

    public void write(int value){
        long stamp= lock.writeLock();
        balance +=value;

        lock.unlockWrite(stamp);


    }

    public void conditionReadWrite(int value) {
        log.info("首先判断balance的值是否符合更新条件。。。");
        long stamp = lock.readLock();
        if (balance > 0) {
            long writeStamp = lock.tryConvertToWriteLock(stamp);
            if (writeStamp != 0) {
                log.info("成功转换为写锁。。。");
                stamp = writeStamp;
                balance += value;
            } else {
                log.info("没有转换写锁，这里需要首先释放读锁，然后再拿拿到写锁。。。");
                lock.unlockRead(stamp);
                log.info("获取写锁");
                stamp = lock.writeLock();
            }
        }
        lock.unlock(stamp);
    }











































}
