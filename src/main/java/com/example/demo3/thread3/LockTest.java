package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LockTest {

    private final Object obj;

    public LockTest(){
        obj=new Object();
    }

    private LockTest demo;

    public void w(){
        demo=new LockTest();
    }

    public void r(){
        LockTest d=demo;
    }


//    private int a;
//    private final int b;
//
//    public LockTest(){
//        b=10;
//        a=10;
//    }
//
//    private LockTest lockTest;
//
//    public void w(){
//        lockTest=new LockTest();
//    }
//
//    public void r(){
//
//    }





//    private int value;
//    public synchronized void a(){
//        value ++;
//    }
//    public synchronized void b(){
//        int a=value;
//        log.info("处理其他操作。。。");
//
//    }

}
