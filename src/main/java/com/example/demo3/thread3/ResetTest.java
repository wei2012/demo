package com.example.demo3.thread3;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResetTest {

    private int a;
    private int b;
    private int c;

    public void a(){
        a =2;
        b=10;

        c=a+b;
        log.info("返回："+c);
    }

//    private int a;
//    private boolean flag;
//
//    public void writer() {
//        log.info("这两个数据之间没有数据依赖性，因此处理器会对这2行代码进行指令重排。");
//        a = 1;
//        flag = true;
//    }
//
//    public void reader(){
//        int b = a + 1;
//        if (flag) {
//            log.info("返回b：" + b);
//        }
//    }

//    private int a;
//    private int b;
//    private int c;
//    public void a(){
//
//        b=2;
//        a =1;
//
//        c=a;
//        b=c+a;
//        log.info("b=:"+b);
//    }

    public static void main(String[] args) {
//        new ResetTest().a();

    }

}
