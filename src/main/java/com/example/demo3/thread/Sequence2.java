package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class Sequence2 {
    private AtomicInteger value=new AtomicInteger(0);

    private int[] ss={2,1,4,6};

    AtomicIntegerArray ai=new AtomicIntegerArray(ss);

    AtomicReference<UserTest> userTest= new AtomicReference<>();

    AtomicIntegerFieldUpdater<UserTest> old=AtomicIntegerFieldUpdater.newUpdater(UserTest.class,"old");


    /**
     * 放在普通方法上，内置锁就是当前类的实例
     *
     * @return
     */
    public int getNext() {
        UserTest userTest=new UserTest();
        log.info(old.getAndIncrement(userTest)+"");
        log.info(old.getAndIncrement(userTest)+"");
        log.info(old.getAndIncrement(userTest)+"");

        ai.getAndIncrement(2);
        ai.getAndAdd(2,10);

        return value.getAndAdd(10);
    }

    /**
     * 修饰静态方法，内置锁是当前的Class字节码对象
     * Sequence.class
     *
     * @return
     */
    public int getPrevious() {
        return value.getAndDecrement();
    }


    public static void main(String[] args) {
        Sequence2 sequence = new Sequence2();

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(Thread.currentThread().getName() + "=========" + sequence.getNext());
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

}
