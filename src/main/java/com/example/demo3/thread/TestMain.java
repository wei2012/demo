package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 测试main方法入口。
 */
@Slf4j
public class TestMain {

private Object obj1=new Object();
private Object obj2 = new Object();


    public static void main(String[] args) {
//        testSingleton();
//        testSync();
//        testSync2();
        //testSync3();
        test33();
    }

    public static void test33(){
        ReadAndWriteLock d = new ReadAndWriteLock();
        d.put("key1", "value1");
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(d.get("key1"));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(d.get("key1"));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(d.get("key1"));
            }
        }).start();
    }

    public void a1 () {
        synchronized (obj1) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (obj2) {
                System.out.println("a1");
            }
        }
    }

    public void b1 () {
        synchronized (obj2) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (obj1) {
                System.out.println("b1");
            }
        }
    }

    public synchronized void a() {
        log.info("输出：a");
        b();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void b() {
        log.info("输出：b");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void testSync3(){
        TestMain d = new TestMain();

        new Thread(new Runnable() {

            @Override
            public void run() {
                d.a1();
            }
        }).start();
        new Thread(new Runnable() {

            @Override
            public void run() {
                d.b1();
            }
        }).start();
    }

    public static void testSync2(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(Thread.currentThread().getName()+",开始执行。。。");
                try {
                    Thread.sleep(new Random().nextInt(2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info(Thread.currentThread().getName()+",执行结束。。。");
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(Thread.currentThread().getName()+",开始执行。。。");
                try {
                    Thread.sleep(new Random().nextInt(2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info(Thread.currentThread().getName()+",执行结束。。。");
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info(Thread.currentThread().getName()+",开始执行。。。");
                try {
                    Thread.sleep(new Random().nextInt(2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info(Thread.currentThread().getName()+",执行结束。。。");
            }
        }).start();

        while(Thread.activeCount() != 1) {
            // 自旋
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("所有的线程执行完毕了...");
    }

    public static void testSync(){
        TestMain t1=new TestMain();
        TestMain t2=new TestMain();

        new Thread(new Runnable() {
            @Override
            public void run() {
                t1.a();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                t2.b();
            }
        }).start();
    }

    public static void testSingleton(){
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 200; i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    log.info(Thread.currentThread().getName() + ":" + Singleton2.getInstance());
                }
            });
        }
        threadPool.shutdown();
    }
}
