package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class Sequence4 {
    private MyLock lock = new MyLock();

    private int value;

    public int getNext() {
        lock.lock();
        value++;
        lock.unlock();
        return value;

    }


    public static void main(String[] args) {
        Sequence4 s = new Sequence4();
        new Thread(new Runnable() {
            @Override
            public void run() {
//                while(true){
//                    log.info("返回："+s.getNext());
//                }
                log.info("返回："+s.getNext());
            }
        }).start();



    }

}
