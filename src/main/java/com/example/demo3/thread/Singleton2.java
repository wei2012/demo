package com.example.demo3.thread;

public class Singleton2 {
    private Singleton2(){}

    private static volatile Singleton2 instance;

    public static Singleton2 getInstance(){
        if(instance==null){
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            synchronized (Singleton2.class){
                if(instance==null){
                    instance=new Singleton2();
                }
            }
        }
        return instance;
    }
}
