package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VolatileTest2 {
    public volatile boolean run=false;

    public static void main(String[] args) {
        VolatileTest2 volatileTest2=new VolatileTest2();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=1;i<=10;i++){
                    log.info("执行了第"+i+"次！");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                volatileTest2.run=true;
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!volatileTest2.run){

                }
                log.info("线程2执行了。。。");
            }
        }).start();
    }
}
