package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class Sequence3 {
    private int value;
    Lock lock=new ReentrantLock();
    Lock l1=new ReentrantLock();

    public int getNext(){
        lock.lock();
        int a= value++;
        lock.unlock();
        return a;
    }


    public static void main(String[] args) {
        Sequence3 sequence = new Sequence3();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    log.info(Thread.currentThread().getName() + "=========" + sequence.getNext());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    log.info(Thread.currentThread().getName() + "=========" + sequence.getNext());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    log.info(Thread.currentThread().getName() + "=========" + sequence.getNext());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

}
