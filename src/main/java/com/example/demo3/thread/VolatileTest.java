package com.example.demo3.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VolatileTest {
    public synchronized int getA() {
        return a;
    }

    public synchronized void setA(int a) {
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.a = a;
    }

    public volatile int a=1;

    public static void main(String[] args) {
        VolatileTest volatileTest=new VolatileTest();
        volatileTest.a=10;
        new Thread(new Runnable() {
            @Override
            public void run() {
                 log.info("返回："+volatileTest.a);
            }
        }).start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("最终返回："+volatileTest.a);
    }

}
