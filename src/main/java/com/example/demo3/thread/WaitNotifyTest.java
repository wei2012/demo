package com.example.demo3.thread;

public class WaitNotifyTest {

    public int getSignal() {
        return signal;
    }

    public void setSignal(int signal) {
        this.signal = signal;
    }

    private volatile int signal;

    public static void main(String[] args) {
        WaitNotifyTest d = new WaitNotifyTest();
        new Thread(new Runnable() {

            @Override
            public void run() {
                System.out.println("修改状态的线程执行...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                d.setSignal(1);
                System.out.println("状态值修改成功。。。");
            }
        }).start();

        new Thread(new Runnable() {

            @Override
            public void run() {

                // 等待signal为1开始执行，否则不能执行
                while(d.getSignal() != 1) {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // 当信号为1 的时候，执行代码
                System.out.println("模拟代码的执行...");


            }
        }).start();

    }


}
