package com.example.demo3.leetcode;

/**
 * 1603. 设计停车系统
 * 请你给一个停车场设计一个停车系统。停车场总共有三种不同大小的车位：大，中和小，每种尺寸分别有固定数目的车位。
 */
public class ParkingSystem {
    int[] carContainer=new int[3];

    public ParkingSystem(int big, int medium, int small) {
        carContainer[0]=big;
        carContainer[1]=medium;
        carContainer[2]=small;
    }

    public boolean addCar(int carType) {
        if(carContainer[carType-1]>0){
            carContainer[carType-1]--;
            return true;
        }
        return false;
    }
}
