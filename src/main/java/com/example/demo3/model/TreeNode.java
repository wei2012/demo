package com.example.demo3.model;

/**
 * 二叉树11
 */
public class TreeNode {
      public int val;
      public TreeNode left;
      public TreeNode right;
      public TreeNode(int x) {
          val = x;
      }
}
