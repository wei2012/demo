package com.example.demo3.cache;

import com.example.demo3.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserCache {

    private static final String CACHE_NAME_B = "cache-b";

    @Cacheable(value = CACHE_NAME_B, key = "'user_'+ #id")
    public UserInfo selectByPrimaryKey(String id) {
        UserInfo userInfo = new UserInfo();
        userInfo.setBillCode("33333");
        userInfo.setAge(33);
        userInfo.setName("哈哈");
        userInfo.setId(1);
        log.info("读取数据数据，第一次。。。");
        return userInfo;
    }
}
