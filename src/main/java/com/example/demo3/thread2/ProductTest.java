package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductTest {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductTest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public ProductTest(int id, String name) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    log.info("开始生产"+name);
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        this.id = id;
        this.name = name;
        log.info(name+"生产完毕。。。");
    }


}
