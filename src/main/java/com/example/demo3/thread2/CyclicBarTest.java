package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

@Slf4j
public class CyclicBarTest {
    Random random=new Random();
    public void meeting(CyclicBarrier barrier){
        try {
            Thread.sleep(random.nextInt(4000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info(Thread.currentThread().getName()+"所有人过来开会。。。");
        if(Thread.currentThread().getName().equals("Thread-7")){
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            barrier.reset();
        }
        try {
            barrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CyclicBarTest demo=new CyclicBarTest();
        CyclicBarrier barrier=new CyclicBarrier(10, new Runnable() {
            @Override
            public void run() {
                log.info("开始开会了。。。");
            }
        });
        for(int i=0;i<10;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    demo.meeting(barrier);
                }
            }).start();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    log.info("barrier.getNumberWaiting:"+barrier.getNumberWaiting());
                    log.info("is broken:"+barrier.isBroken());
                }
            }
        }).start();
    }








































}
