package com.example.demo3.thread2;


import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class CountDownLatchTest {
    private int[] nums;

    public CountDownLatchTest(int line) {
        nums = new int[line];
    }

    public void calc(String line, int index, CountDownLatch latch) {
        String[] nus = line.split(",");
        int total = 0;
        for (String num : nus) {
            total += Integer.parseInt(num);
        }
        nums[index] = total;
        log.info(Thread.currentThread().getName() + "=====" + line + "=====" + total);
        latch.countDown();
    }

    public void sum() {
        log.info("开始汇总。。。");
        int total = 0;
        for (int i = 0; i < nums.length; i++) {
            total += nums[i];
        }
        log.info("汇总结果：" + total);
    }

    public static void main(String[] args) {
        List<String> contents = readFile();
        int lineCount = contents.size();
        CountDownLatch latch = new CountDownLatch(lineCount);
        CountDownLatchTest d = new CountDownLatchTest(lineCount);
        for (int i = 0; i < lineCount; i++) {
            final int j = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    d.calc(contents.get(j), j, latch);
                }
            }).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        d.sum();
    }

    private static List<String> readFile() {
        List<String> contents = new ArrayList<>();
        String line = null;
        BufferedReader br = null;
        try {
            try {
                br = new BufferedReader(new FileReader("D:\\work\\temp\\nums.txt"));
                while ((line = br.readLine()) != null) {
                    contents.add(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return contents;
    }


}
