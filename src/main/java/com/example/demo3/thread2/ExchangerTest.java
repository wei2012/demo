package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Exchanger;

@Slf4j
public class ExchangerTest {
    public void a(Exchanger<String> exch){
        log.info("a 方法执行。。。");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String res="123451";
        try {
            log.info("等待对比结果。。。");
            exch.exchange(res);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void b(Exchanger<String> exch){
        log.info("b 方法开始执行");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String res="12345";
        try {
            String a= exch.exchange(res);
            log.info("开始进行比对。。。。");
            log.info("比对结果为："+a.equals(res));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ExchangerTest d =new ExchangerTest();
        Exchanger<String> exch = new Exchanger<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                d.a(exch);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                d.b(exch);
            }
        }).start();
    }








































}
