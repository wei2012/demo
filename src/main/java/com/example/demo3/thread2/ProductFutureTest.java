package com.example.demo3.thread2;

public class ProductFutureTest {
    private ProductTest product;
    private boolean down;
    public synchronized void setProduct(ProductTest product){
        if(down){
            return;
        }
        this.product=product;
        this.down=true;
        notifyAll();

    }

    public synchronized ProductTest get(){
        while (!down){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return product;
    }






}
