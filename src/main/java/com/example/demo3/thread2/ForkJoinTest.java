package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.*;

@Slf4j
public class ForkJoinTest extends RecursiveTask<Integer> {

    private int begin;
    private int end;

    public ForkJoinTest(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    protected Integer compute() {
        log.info(Thread.currentThread().getName() + " ... ");
        int sum=0;
        if(end-begin <= 2){
            for(int i=begin;i<=end;i++){
                sum+=i;
            }
        }
        else{
            ForkJoinTest d1=new ForkJoinTest(begin,(begin+end)/2);
            ForkJoinTest d2=new ForkJoinTest((begin+end)/2+1,end);
            d1.fork();
            d2.fork();
            Integer a= d1.join();
            Integer b= d2.join();
            sum=a+b;
        }
        return sum;
    }

    public static void main(String[] args) throws Exception {
//        ForkJoinPool pool=new ForkJoinPool(20);
//        Future<Integer> future= pool.submit(new ForkJoinTest(1,100000));
//        log.info("...");
//        log.info("计算值为："+future.get());

//        ArrayList<String> s=new ArrayList<>();
//        Collections.synchronizedList(s);
//
//        HashMap<String,Object> res=new HashMap<>();
//        Collections.synchronizedMap(res);

        CopyOnWriteArrayList<String> s=new CopyOnWriteArrayList<>();
    }

}
