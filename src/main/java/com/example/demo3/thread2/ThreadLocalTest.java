package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadLocalTest {
    private ThreadLocal<Integer> count = new ThreadLocal<Integer>() {

        @Override
        protected Integer initialValue() {
            return new Integer("0");
        }
    };

    public int getNext() {
        Integer value = count.get();
        value++;
        count.set(value);
        return value;
    }

    public static void main(String[] args) {
        ThreadLocalTest demo = new ThreadLocalTest();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    log.info(Thread.currentThread().getName() + "===" + demo.getNext());
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    log.info(Thread.currentThread().getName() + "===" + demo.getNext());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
