package com.example.demo3.thread2;

public class ConditionTest {
    private int signal;

    public synchronized void a() {
        while(signal != 0 ) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("a");
        signal ++;
        notifyAll();
    }

    public synchronized void b() {
        while(signal != 1) {
            try {
                wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("b");
        signal ++;
        notifyAll();
    }

    public synchronized void c () {
        while(signal != 2) {
            try {
                wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("c");
        signal = 0;
        notifyAll();
    }

    public static void main(String[] args) {

        ConditionTest d = new ConditionTest();
        A a = new A(d);
        B b = new B(d);
        C c = new C(d);

        new Thread(a).start();
        new Thread(b).start();
        new Thread(c).start();

    }
}

class A implements Runnable {

    private ConditionTest ConditionTest;

    public A(ConditionTest ConditionTest) {
        this.ConditionTest = ConditionTest;
    }

    @Override
    public void run() {
        while(true) {
            ConditionTest.a();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
class B implements Runnable {

    private ConditionTest ConditionTest;

    public B(ConditionTest ConditionTest) {
        this.ConditionTest = ConditionTest;
    }

    @Override
    public void run() {
        while(true) {
            ConditionTest.b();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
class C implements Runnable {

    private ConditionTest ConditionTest;

    public C(ConditionTest ConditionTest) {
        this.ConditionTest = ConditionTest;
    }

    @Override
    public void run() {
        while(true) {
            ConditionTest.c();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
