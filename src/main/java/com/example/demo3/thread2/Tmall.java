package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class Tmall implements ShopTest {
	public final int MAX_COUNT=10;

	private BlockingQueue<Integer> queue=new ArrayBlockingQueue<>(MAX_COUNT);

	@Override
	public void push(){
		try {
			queue.put(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void take(){
		try {
			queue.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void size(){
		while (true){
			log.info("长度："+queue.size());
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
