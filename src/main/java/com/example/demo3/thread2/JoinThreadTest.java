package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JoinThreadTest {
    public void a(Thread joinThread){
      log.info("方法a执行了。。。");
      joinThread.start();
      try
      {
          joinThread.join();
      }
      catch (InterruptedException e){
          e.printStackTrace();
      }
      log.info("方法a执行完毕。。。");
    }

    public void b(){
        log.info("加载线程开始执行。。。");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("加载线程执行完毕。。。");
    }

    public static void main(String[] args) {
        JoinThreadTest demo= new JoinThreadTest();
        Thread joinThread = new Thread(new Runnable() {
            @Override
            public void run() {
                demo.b();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                demo.a(joinThread);
            }
        }).start();
    }
}
