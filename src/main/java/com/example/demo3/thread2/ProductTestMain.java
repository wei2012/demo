package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductTestMain {
    public static void main(String[] args) {
        ProductFactoryTest pf = new ProductFactoryTest();
        ProductFutureTest f = pf.createProduct("蛋糕");
        log.info("我要去上班了，下了班我来取蛋糕。。。");
        log.info("我拿蛋糕回家。。。" + f.get());
    }
}
