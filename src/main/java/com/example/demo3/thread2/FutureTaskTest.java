package com.example.demo3.thread2;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

@Slf4j
public class FutureTaskTest {
    public static void main(String[] args) throws Exception {
        Callable<Integer> call =new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.info("正在计算结果。。。");
                Thread.sleep(3000);
                return 1;
            }
        };
        FutureTask<Integer> task =new FutureTask<>(call);
        Thread thread=new Thread(task);
        thread.start();
        log.info("干点别的。。。");
        Integer result=task.get();
        log.info("拿到的结果为："+result);
    }
}
