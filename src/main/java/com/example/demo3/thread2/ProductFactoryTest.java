package com.example.demo3.thread2;

import java.util.Random;
import java.util.concurrent.Future;

public class ProductFactoryTest {
    public ProductFutureTest createProduct(String name) {
        ProductFutureTest f = new ProductFutureTest();
        new Thread(new Runnable() {
            @Override
            public void run() {
                ProductTest p = new ProductTest(new Random().nextInt(), name);
                f.setProduct(p);
            }
        }).start();
        return f;
    }
}
