package com.example.demo3;
import com.example.demo3.common.ThreadSemaphore;
import com.example.demo3.zk.Key;
import com.google.common.collect.Maps;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class TestSemaphore extends Demo3ApplicationTests{
    private static final int SEM_MAX = 10;

    @Test
    public void testContainsDuplicate() {
        int[] intput = new int[]{1, 2, 3, 4};
        boolean re = containsDuplicate2(intput);
        System.out.println(re);
    }

    /**
     * 217. 存在重复元素
     * 如果任意一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false 。
     * @param nums
     * @return
     */
    public boolean containsDuplicate2(int[] nums) {
        Arrays.sort(nums);
        for(int i=0;i<nums.length-1;i++){
            if(nums[i]==nums[i+1]){
                return true;
            }
        }
        return false;
    }

    /**
     * 217. 存在重复元素
     * 如果任意一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false 。
     * @param nums
     * @return
     */
    public boolean containsDuplicate(int[] nums) {
        HashMap<Integer,Integer> map= new HashMap<Integer, Integer>();
        for(int i=0;i<nums.length;i++){
            if(map.containsKey(nums[i])){
                return true;
            }
            else{
                map.put(nums[i],1);
            }
        }
        return false;
    }


    public boolean lemonadeChange3(int[] bills) {
        int five = 0, ten = 0;
        for (int bill : bills) {
            if (bill == 5) {
                five++;
            } else if (bill == 10) {
                if (five > 0) {
                    five--;
                    ten++;
                } else {
                    return false;
                }
            } else {
                if (five > 0 && ten > 0) {
                    five--;
                    ten--;
                } else if (five >= 3) {
                    five -= 3;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Test
    public void testLemonadeChange2(){
        int[] bills=new int[]{5,5,10,10,20};
        boolean result= lemonadeChange2(bills);
        System.out.println(result);
    }

    /**
     * 860. 柠檬水找零
     * 在柠檬水摊上，每一杯柠檬水的售价为 5 美元。
     * 顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。
     * 每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。
     * 注意，一开始你手头没有任何零钱。
     * 如果你能给每位顾客正确找零，返回 true ，否则返回 false 。
     * @param bills
     * @return
     */
    public boolean lemonadeChange2(int[] bills) {
        int five = 0;
        int ten = 0;
        for (int bill : bills) {
            if (bill == 5) {
                five++;
            } else if (bill == 10) {
                if (five == 0) {
                    return false;
                }
                ten++;
                five--;
            } else {
                if (five > 0 && ten > 0) {
                    five--;
                    ten--;
                } else if (five >= 3) {
                    five -= 3;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Test
    public void testGenerateTriangle(){
        List<List<Integer>> result= generate(5);
        System.out.println(StringUtils.join(result,"|"));
    }

    /**
     * 118. 杨辉三角
     * 给定一个非负整数 numRows，生成杨辉三角的前 numRows 行。
     * @param numRows
     * @return
     */
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ret=new ArrayList<List<Integer>>();
        for(int i=0;i<numRows;++i){
            List<Integer> row=new ArrayList<Integer>();
            for(int j=0;j<=i;++j){
                if(j==0 || j==i){
                    row.add(1);
                }
                else{
                    row.add(ret.get(i-1).get(j-1)+ret.get(i-1).get(j));
                }
            }
            ret.add(row);
        }
        return ret;
    }

    @Test
    public void testRunningSum() {
        int[] re=new int[]{1,2,3,4};
        int[] out= runningSum2(re);
        System.out.println(ArrayUtils.toString(out, ","));
    }

    public int[] runningSum2(int[] nums) {
        int sum = 0;
        for (int n = 0; n < nums.length; n++) {
            sum += nums[n];
            nums[n] = sum;
        }
        return nums;
    }


    /**
     * 1480. 一维数组的动态和
     * 给你一个数组 nums 。数组「动态和」的计算公式为：runningSum[i] = sum(nums[0]…nums[i]) 。
     * 请返回 nums 的动态和。
     * @param nums
     * @return
     */
    public int[] runningSum(int[] nums) {
        int[] re=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            for(int j=0;j<=i;j++){
                re[i]+=nums[j];
            }
            System.out.println(re[i]);
        }
        return re;
    }

    /**
     * 四种解法+图解 188.买卖股票的最佳时机 IV
     * @param k
     * @param prices
     * @return
     */
    public int maxProfix2(int k,int[] prices){
        if(prices==null || prices.length==0){
            return 0;
        }
        int n=prices.length;
        //当k非常大时转为无限次交易
        if(k>=n/2){
            int dp0=0,dp1=-prices[0];
            for(int i=1;i<n;++i){
                int tmp=dp0;
                dp0=Math.max(dp0,dp1+prices[i]);
                dp1=Math.max(dp1,dp0-prices[i]);
            }
            return Math.max(dp0,dp1);
        }
        Map<Key,Integer> cache=new HashMap<Key,Integer>();
        return dfs2(cache,0,0,0,k,prices);
    }

    /**
     * 带记忆化的 计算k次交易，代码和递归版的一样只是前后加了缓存
     */
    public int dfs2(Map<Key,Integer> cache,int index,int status,int count,int k,int[] prices){
        Key key=new Key(index,status,count);
        if(cache.containsKey(key)){
            return cache.get(key);
        }
        if(index==prices.length||count==k){
            return 0;
        }
        int a=0,b=0,c=0;
        a=dfs2(cache,index+1,status,count,k,prices);
        if(status==1){
            b=dfs2(cache, index+1, 0, count+1, k, prices)+prices[index];
        }
        else{
            c=dfs2(cache,index+1,1,count,k,prices)-prices[index];
        }
        cache.put(key,Math.max(Math.max(a,b),c));
        return cache.get(key);
    }


    @Test
    public void testSemaphore() {
        Semaphore sem = new Semaphore(SEM_MAX);
        //创建线程池
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        //在线程池中执行任务
        threadPool.execute(new ThreadSemaphore(sem, 5));
        threadPool.execute(new ThreadSemaphore(sem, 4));
        threadPool.execute(new ThreadSemaphore(sem, 7));
        //关闭池
        threadPool.shutdown();
    }
}
