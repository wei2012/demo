package com.example.demo3;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.time.Clock;
import java.time.LocalDate;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class TestCountDownLatch extends Demo3ApplicationTests {

    @Test
    public void aa() {
        log.info("hello world");
//        int i = 15, j = 2;
//        System.out.println("i ^ j = " + (i ^ j));
    }

    @Test
    public void testCountDownLatch() {
        final CountDownLatch latch = new CountDownLatch(2);

        new Thread() {
            public void run() {
                try {
                    System.out.println("子线程1" + Thread.currentThread().getName() + "正在执行");
                    Thread.sleep(3000);
                    System.out.println("子线程2" + Thread.currentThread().getName() + "执行完毕");
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            ;
        }.start();

        new Thread() {
            public void run() {
                try {
                    System.out.println("子线程3" + Thread.currentThread().getName() + "正在执行");
                    Thread.sleep(3000);
                    System.out.println("子线程4" + Thread.currentThread().getName() + "执行完毕");
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            ;
        }.start();

        try {
            System.out.println("等待2个子线程执行完毕...");
            latch.await();
            System.out.println("2个子线程已经执行完毕");
            System.out.println("继续执行主线程");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCountDownLatch2() throws InterruptedException {
        CountDownLatch countDown = new CountDownLatch(1);
        CountDownLatch await = new CountDownLatch(5);

        // 依次创建并启动处于等待状态的5个MyRunnable线程
        for (int i = 0; i < 20; ++i) {
            new Thread(new MyRunnable(countDown, await, i)).start();
        }
        countDown.countDown();
        await.await();
        System.out.println("Bingo!");
    }

    class MyRunnable implements Runnable {

        private final CountDownLatch countDown;
        private final CountDownLatch await;
        private final int num;

        public MyRunnable(CountDownLatch countDown, CountDownLatch await, int num) {
            this.num = num;
            this.countDown = countDown;
            this.await = await;
        }

        @Override
        public void run() {
            try {
                countDown.await();//等待主线程执行完毕，获得开始执行信号...
                System.out.println("处于等待的线程" + num + "开始自己预期工作......");
                Thread.sleep(200);
                await.countDown();//完成预期工作，发出完成信号...
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //经典案例代码：
    private static final int _1MB = 1024 * 1024;
    /**
     * VM参数：-verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     */
    @Test
    public void testAllocation() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB];
        allocation4 = new byte[4 * _1MB];  // 出现一次Minor GC
    }

    @Test
    public void testLambda(){
        final Clock clock = Clock.systemUTC();
        final LocalDate date = LocalDate.now();
        final LocalDate dateFromClock = LocalDate.now( clock );

        System.out.println( date );
        System.out.println( dateFromClock );

//        Arrays.asList( "a", "b", "d" ).forEach(e -> {
//            System.out.print( e );
//            System.out.print( e );
//        } );
    }


}
