package com.example.demo3;

import com.example.demo3.zk.ListNode;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TestPalindrome extends Demo3ApplicationTests  {

    /**
     * 416. 分割等和子集
     * 给定一个只包含正整数的非空数组。是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。
     *
     * 注意:
     *
     * 每个数组中的元素不会超过 100
     * 数组的大小不会超过 200
     * @param nums
     * @return
     */
    public boolean canPartition(int[] nums){
        int n=nums.length;
        if(n<2){
            return false;
        }
        int sum=0,maxNum=0;
        for(int num:nums){
            sum +=num;
            maxNum=Math.max(maxNum,num);
        }
        if(sum%2!=0){
            return false;
        }
        int target=sum/2;
        if(maxNum>target){
            return false;
        }
        boolean[][] dp=new boolean[n][target+1];
        for(int i=0;i<n;i++){
            dp[i][0]=true;
        }
        dp[0][nums[0]]=true;
        for(int i=1;i<n;i++){
            int num=nums[i];
            for(int j=1;j<=target;j++){
                if(j>=num){
                    dp[i][j]=dp[i-1][j]|dp[i-1][j-num];
                }
                else{
                    dp[i][j]=dp[i-1][j];
                }
            }
        }
        return dp[n-1][target];
    }

    /**
     * 4. 寻找两个正序数组的中位数
     * @param nums1
     * @param nums2
     */
    public double  findMedianSortedArrays(int[] nums1, int[] nums2){
        int length1=nums1.length,length2=nums2.length;
        int totalLength=length1+length2;
        if(totalLength%2==1){
            int midIndex=totalLength/2;
            double median=getKthElement(nums1,nums2,midIndex+1);
            return median;
        }
        else{
            int midIndex1=totalLength/2-1,midIndex2=totalLength/2;
            double median=(getKthElement(nums1,nums2,midIndex1+1)+getKthElement(nums1,nums2,midIndex2+1))/2.0;
            return median;
        }
    }

    public int getKthElement(int[] nums1,int[] num2,int k){
        /* 主要思路：要找到第 k (k>1) 小的元素，那么就取 pivot1 = nums1[k/2-1] 和 pivot2 = nums2[k/2-1] 进行比较
         * 这里的 "/" 表示整除
         * nums1 中小于等于 pivot1 的元素有 nums1[0 .. k/2-2] 共计 k/2-1 个
         * nums2 中小于等于 pivot2 的元素有 nums2[0 .. k/2-2] 共计 k/2-1 个
         * 取 pivot = min(pivot1, pivot2)，两个数组中小于等于 pivot 的元素共计不会超过 (k/2-1) + (k/2-1) <= k-2 个
         * 这样 pivot 本身最大也只能是第 k-1 小的元素
         * 如果 pivot = pivot1，那么 nums1[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums1 数组
         * 如果 pivot = pivot2，那么 nums2[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums2 数组
         * 由于我们 "删除" 了一些元素（这些元素都比第 k 小的元素要小），因此需要修改 k 的值，减去删除的数的个数
         */
        int length1=nums1.length,length2=num2.length;
        int index1=0,index2=0;
        int kthElement=0;
        while (true){
            // 边界情况
            if(index1==length1){
                return num2[index2+k-1];
            }
            if(index2==length2){
                return nums1[index1+k-1];
            }
            if(k==1){
                return Math.min(nums1[index1],num2[index2]);
            }
            // 正常情况
            int half=k/2;
            int newIndex1=Math.min(index1+half,length1)-1;
            int newIndex2=Math.min(index2+half,length2)-1;
            int pivot1=nums1[newIndex1],pivot2=num2[newIndex2];
            if(pivot1<pivot2){
                k-=(newIndex1-index1+1);
                index1=newIndex1+1;
            }
            else{
                k-=(newIndex2-index2+1);
                index2=newIndex2+1;
            }
        }
    }

    public ListNode detectCycle2(ListNode head){
        if(head==null){
            return null;
        }
        ListNode slow=head,fast=head;
        while (fast!=null){
            slow=slow.next;
            if(fast.next!=null){
                fast=fast.next.next;
            }
            else{
                return null;
            }
            if(fast==slow){
                ListNode ptr=head;
                while (ptr!=slow){
                    ptr=ptr.next;
                    slow=slow.next;
                }
                return ptr;
            }
        }
        return null;
    }

    /**
     * 142 环形链表 II
     * 一个非常直观的思路是：我们遍历链表中的每个节点，并将它记录下来；一旦遇到了此前遍历过的节点，就可以判定链表中存在环。借助哈希表可以很方便地实现。
     * @param head
     * @return
     */
    public ListNode detectCycle(ListNode head){
        ListNode pos=head;
        Set<ListNode> visited=new HashSet<ListNode>();
        while(pos!=null){
            if(visited.contains(pos)){
                return pos;
            }
            else{
                visited.add(pos);
            }
            pos=pos.next;
        }
        return null;
    }

    @Test
    public void testRomanInt(){
        int a=romanToInt("XXVII");
        System.out.println(a);
    }

    /**
     * 罗马数字转数字。
     * @param s
     * @return
     */
    public int romanToInt(String s){
        int sum=0;
        int preNum=getValue(s.charAt(0));
        for(int i=1;i<s.length();i++){
            int num=getValue(s.charAt(i));
            if(preNum<num){
                sum-=preNum;
            }
            else{
                sum+=preNum;
            }
            preNum=num;
        }
        sum+=preNum;
        return sum;
    }

    private int getValue(char ch){
        switch (ch){
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D':  return 500;
            case 'M': return 1000;
            default:return 0;
        }
    }

    public String longestPalindrome(String s){
        int n=s.length();
        boolean[][] dp=new boolean[1][1];
        String ans="";
        for(int l=0;l<n;++l){
            for(int i=0;i+1<n;++i){
                int j=i+1;
                if(l==0){
                    dp[i][j]=true;
                }
                else if(l==1){
                    dp[i][j]=(s.charAt(i)==s.charAt(j));
                }
                else{
                    dp[i][j]=(s.charAt(i)==s.charAt(j) && dp[i+1][j-1]);
                }
                if(dp[i][j] && l+1 >ans.length()){
                    ans=s.substring(i,i+l+1);
                }
            }
        }
        return ans;
    }

    

}
