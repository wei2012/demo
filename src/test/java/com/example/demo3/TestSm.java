package com.example.demo3;

import com.alibaba.fastjson.JSON;
import com.example.demo3.model.UserInfo;
import com.example.demo3.sm4.SM4Utils;
import org.dozer.DozerBeanMapper;
import org.junit.Test;

public class TestSm extends Demo3ApplicationTests {

    private static final DozerBeanMapper mapper = new DozerBeanMapper();

    @Test
    public void test1() {
        String plainText = "ererfeiisgod";

        SM4Utils sm4 = new SM4Utils();
        //必须16位字符。
        sm4.secretKey = "JeF8U9wHFOMfs2Y8";
        sm4.hexString = false;

        System.out.println("ECB模式加密");
        String cipherText = sm4.encryptData_ECB(plainText);
        System.out.println("密文: " + cipherText);
        System.out.println("");

        plainText = sm4.decryptData_ECB(cipherText);
        System.out.println("明文: " + plainText);
        System.out.println("");

        System.out.println("CBC模式加密");
        sm4.iv = "UISwD9fW6cFh9SNS";
        cipherText = sm4.encryptData_CBC(plainText);
        System.out.println("密文: " + cipherText);
        System.out.println("");

        plainText = sm4.decryptData_CBC(cipherText);
        System.out.println("明文: " + plainText);

        System.out.println("CBC模式解密");
        System.out.println("密文：4esGgDn/snKraRDe6uM0jQ==");
        String cipherText2 = "4esGgDn/snKraRDe6uM0jQ==";
        plainText = sm4.decryptData_CBC(cipherText2);
        System.out.println("明文: " + plainText);
    }


    @Test
    public void test2() {
        UserInfo userInfo=new UserInfo();
        userInfo.setId(1);
        UserInfo userInfo2=new UserInfo();
        mapper.map(userInfo, userInfo2);
        System.out.println("userInfo2:" + JSON.toJSONString(userInfo2));
    }

}
