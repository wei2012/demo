package com.example.demo3;

import com.alibaba.fastjson.JSON;
import com.example.demo3.mapper.UserInfoMapper;
import com.example.demo3.model.UserInfo;
import com.example.demo3.model.UserInfoExample;
import jdk.nashorn.internal.runtime.JSONFunctions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Random;

public class TestPartition extends Demo3ApplicationTests {

    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * 准备工作，插入数据，100000条记录。
     */
    @Test
    public void test11() {
        for (int i = 0; i < 100000; i++) {
            UserInfo userInfo = new UserInfo();
            //生成随机数单号(1到100000)。
            Random random = new Random();
            int max = 100000;
            int min = 1;
            int s = random.nextInt(max) % (max - min + 1) + min;

            userInfo.setAge(random.nextInt(90) + 10);
            userInfo.setName("张三" + i);
            userInfo.setBillCode("" + s);
            userInfoMapper.insertSelective(userInfo);
        }
    }

    /**
     * 查询数据,如查询1000个单号，每300个拆分。最后把结果集合并。
     */
    @Test
    public void test12() {
        //结果集
        List<UserInfo> result=Lists.newArrayList();
        List<String> listSearch = Lists.newArrayList();
        for (int i = 1; i <= 1000; i++) {
            listSearch.add(i+"");
        }
        //按每300个拆分。只需查询3次数据库，用in查询。
        List<List<String>> listOne = Lists.partition(listSearch, 300);
        for (List<String> one : listOne) {
            UserInfoExample example=new UserInfoExample();
            example.createCriteria().andBillCodeIn(one);
            List<UserInfo> oneUserInfo= userInfoMapper.selectByExample(example);
            result.addAll(oneUserInfo);
        }
        //输出
        for(UserInfo userInfo:result){
            System.out.println("显示:"+JSON.toJSONString(userInfo));
        }
    }

}
