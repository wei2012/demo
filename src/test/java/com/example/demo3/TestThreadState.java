package com.example.demo3;

import com.example.demo3.common.ThreadExecutor;
import com.example.demo3.common.ThreadReturnBack;
import com.example.demo3.common.ThreadState;
import com.example.demo3.common.ThreadTest;
import com.example.demo3.configuration.Config;
import com.example.demo3.thread.Singleton;
import com.example.demo3.thread.Singleton2;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * 多线程测试。
 */
@Slf4j
public class TestThreadState extends Demo3ApplicationTests {

    @Test
    public void testThreadState(){
        ThreadState n = new ThreadState();
        Thread thread=new Thread(n);
        thread.start();

        while (true){
            synchronized (n) {
                log.info("主线程执行了。。。");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                n.notifyAll();
            }
        }

    }

    @Test
    public void test22(){
        ThreadTest threadTest=new ThreadTest("first-thread");
        ThreadTest threadTest2=new ThreadTest("second-thread");
//        ThreadTest threadTest=new ThreadTest();
//        ThreadTest threadTest2=new ThreadTest();

//        threadTest.setDaemon(true);
//        threadTest2.setDaemon(true);

        threadTest.start();
        threadTest2.start();

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        threadTest.interrupt();

    }

    @Test
    public void test21(){
        Thread thread=new Thread(new ThreadTest());
        thread.start();
    }

    @Test
    public void test23(){
//        new Thread(){
//            public void run(){
//                log.info("thread start...");
//            };
//        }.start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                log.info("thread start...");
//            }
//        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("runnable");
            }
        }){
            public void run(){
                log.info("sub");
            }
        }.start();
    }

    /**
     * 测试退回
     */
    @Test
    public void testReturnBack() throws Exception {
        ThreadReturnBack t =new ThreadReturnBack();
        FutureTask<Integer> task=new FutureTask<>(t);
        Thread th = new Thread(task);
        th.start();
        log.info("我先干点别的。。。");
        Integer re= task.get();
        log.info("返回结果：{}",re);
    }

    /**
     * 测试定时任务
     */
    @Test
    public void testTimeTask() throws Exception {
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //实现定时任务
                log.info("timer task is run");
            }
        },0,1000);
    }

    /**
     * 测试线程池
     */
    @Test
    public void testExecutor() throws Exception {
        Executor threadPool = Executors.newCachedThreadPool();
        for(int i=0;i<100;i++){
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    log.info(Thread.currentThread().getName());
                }
            });
        }
        ((ExecutorService) threadPool).shutdown();

    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testAsync() throws Exception {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(Config.class);
        ThreadExecutor te= ac.getBean(ThreadExecutor.class);
        te.a();
        te.b();
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testStream() {
        List<Integer> values= Arrays.asList(10,20,30,40);
        int res= new TestThreadState().add(values);
        log.info("计算结果为："+res);
    }

    /**
     * 求和
     * @param values
     * @return
     */
    public int add(List<Integer> values) {
        //values.parallelStream().forEach(System.out::println);
        //values.stream().forEach(System.out::println);
        //values.stream().forEachOrdered(System.out::println);
        return values.parallelStream().mapToInt(a -> a * 2).sum();
        //return 0;
    }

    @Test
    public void test24(){
//        Thread t1 =  new Thread(new Target());
//        Thread t2 =  new Thread(new Target());
//        t1.setPriority(10);
//        t2.setPriority(1);
//
//        t1.start();
//        t2.start();

        while (true){
            log.info("hello world！");
        }
    }

    @Test
    public void test25(){
        Sequence sequence=  new Sequence();
//        while (true){
//            log.info(sequence.getNext()+"");
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    log.info(Thread.currentThread().getName()+"========="+sequence.getNext());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    log.info(Thread.currentThread().getName()+"========="+sequence.getNext());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    log.info(Thread.currentThread().getName()+"========="+sequence.getNext());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("结果："+sequence.xx());
    }

    @Test
    public void test26(){
        Singleton2 s1=Singleton2.getInstance();
        Singleton2 s2= Singleton2.getInstance();
        Singleton2 s3=Singleton2.getInstance();
        Singleton2 s4=Singleton2.getInstance();
        log.info("返回:{}",s1);
        log.info("返回:{}",s2);
        log.info("返回:{}",s3);
        log.info("返回:{}",s4);
    }



    public class Sequence {

        private int value;

        /**
         * synchronized 放在普通方法上，内置锁就是当前类的实例
         *
         * @return
         */
        public synchronized int getNext() {
            return value++;
        }

        /**
         * 修饰静态方法，内置锁是当前的Class字节码对象
         * Sequence.class
         *
         * @return
         */
        public synchronized int getPrevious() {
            return value--;
        }

        public int xx(){
            synchronized (Sequence.class){
                if(value>0){
                    return value;
                }
                else{
                    return -1;
                }
            }
        }


    }

    public class Target implements Runnable {

        @Override
        public void run() {

            while(true) {
                System.out.println(Thread.currentThread().getName() + " ...");
//			Thread.sleep(1);
            }

        }

    }

}


