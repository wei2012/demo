package com.example.demo3;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.*;

public class TestAlgo extends Demo3ApplicationTests {

    @Test
    public void testLargestPerimeter(){
        int[] nums=new int[]{9,1,2,10};
        int re=largestPerimeter(nums);
        System.out.println(re);
    }

    /**
     * 976. 三角形的最大周长
     * 给定由一些正数（代表长度）组成的数组 A，返回由其中三个长度组成的、面积不为零的三角形的最大周长。
     *
     * 如果不能形成任何面积不为零的三角形，返回 0。
     * @param a
     * @return
     */
    public int largestPerimeter(int[] a){
        Arrays.sort(a);
        for(int i=a.length-1;i>=2;--i){
            if(a[i-2]+a[i-1]>a[i]){
                return a[i-2]+a[i-1]+a[i];
            }
        }
        return 0;
    }

    @Test
    public void testReversePairs(){
        int[] nums=new int[]{1,3,2,3,1};
        int re=reversePairs2(nums);
        System.out.println(re);
    }

    /**
     * 493. 翻转对
     * 归并排序解法
     * 前面的暴力解法的时间复杂度为 O ( n 2 ) O(n^2)O(n
     * 2
     *  )，使用归并排序的思路可以把时间复杂度提高到 n log ⁡ ( n ) n\log(n)nlog(n)。其主要思路是，如果把数组中的 nums[left...right]，划分为两个子数组 nums[left...mid] 和 nums[mid+1...right]，
     *  在这两个子数组都排好序的情况下，我们可以很方便地计算出对于左边子数组中的某个元素
     *  nums[i] 符合 num[i]>nums[j]*2 （nums[j]是右边子数组中的元素）的个数。
     *  由于两个子数组都是已经排好序的，所以符合条件的 j一定是从右边子数组的左端点开始的，逐渐向右直到不满足条件。并且较大的 i 会完全包含较小的 i 的集合。
     * 翻转对
     * @param nums
     * @return
     */
    public int reversePairs2(int[] nums){
        if(nums.length<2){
            return 0;
        }
        int count=mergeCount(nums,0,nums.length-1);
        return count;
    }

    private int merge(int nums[],int left,int mid,int right) {
        int count = 0;
        int lastCount = 0;
        int j = mid + 1;
        int i = left;
        while (j <= right && i <= mid) {
            if (i > mid) {
                break;
            }
            long ni = (long) nums[i];
            long nj = (long) nums[j];
            if (ni > nj * 2) {
                lastCount++;
                if (j == right) {
                    count += lastCount * (mid - i + 1);
                }
                j++;
            } else {
                count += lastCount;
                i++;
            }
        }
        int[] leftNums = new int[mid - left + 1];
        int[] rightNums = new int[right - mid];
        for (int a = 0; a < mid - left + 1; a++) {
            leftNums[a] = nums[left + a];
        }
        for (int a = 0; a < rightNums.length; a++) {
            rightNums[a] = nums[mid + a + 1];
        }
        int index1 = 0;
        int index2 = 0;
        for (int a = left; a <= right; a++) {
            // 如果左右两个数组有一个已经完全归并到整体中
            if (index1 >= leftNums.length) {
                for (int b = a; b <= right; b++) {
                    nums[b] = rightNums[index2++];
                }
                break;
            } else if (index2 >= rightNums.length) {
                for (int b = a; b <= right; b++) {
                    nums[b] = leftNums[index1++];
                }
                break;
            }
            if (leftNums[index1] < rightNums[index2]) {
                nums[a] = leftNums[index1++];
            } else {
                nums[a] = rightNums[index2++];
            }
        }
        return count;
    }

    private int mergeCount(int[] nums, int left, int right) {
        int count=0;
        if(left>=right){
            return 0;
        }
        int mid=(left+right)/2;
        int countLeft=mergeCount(nums,left,mid);
        int countRight=mergeCount(nums,mid+1,right);
        int countMerge=merge(nums,left,mid,right);
        count=countLeft+countRight+countMerge;
        return count;
    }


    /**
     * 翻转对
     * @param nums
     * @return
     */
    public int reversePairs(int[] nums){
        if(nums.length<2){
            return 0;
        }
        int count=0;
        for(int i=nums.length-1;i>0;i--){
            for(int j=i-1;j>=0;j--){
                long nj =(long) nums[j];
                long ni=(long)nums[i];
                if(nj>ni){
                    count++;
                }
            }
        }
        return count;
    }

    @Test
    public void testLetterCombinations(){
        List<String> re=letterCombinations("56");
        System.out.println(JSON.toJSONString(re));
    }

    /**
     * 17. 电话号码的字母组合
     * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。
     * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
     */
    public List<String> letterCombinations(String digits) {
        List<String> combinations = new ArrayList<String>();
        if (digits.length() == 0) {
            return combinations;
        }
        Map<Character, String> phoneMap = new HashMap<Character, String>() {{
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }};
        backtrack(combinations, phoneMap, digits, 0, new StringBuffer());
        return combinations;
    }

    public void backtrack(List<String>combinations,Map<Character,String> phoneMap,
                          String digits,int index,StringBuffer combination){
        if(index==digits.length()){
            combinations.add(combination.toString());
        }
        else{
            char digit=digits.charAt(index);
            String letters=phoneMap.get(digit);
            int lettersCount=letters.length();
            for(int i=0;i<lettersCount;i++){
                combination.append(letters.charAt(i));
                backtrack(combinations,phoneMap,digits,index+1,combination);
                combination.deleteCharAt(index);
            }
        }
    }

    @Test
    public void testMaxProfit5(){
        int[] prices=new int[]{7,1,5,3,6,4};
        int re=maxProfit5(prices);
        System.out.println(re);
    }

    /**
     * 122. 买卖股票的最佳时机 II
     * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
     *
     * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
     *
     * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
     * @param prices
     * @return
     */
    public int maxProfit5(int[] prices){
        int n=prices.length;
        int[][] dp=new int[n][2];
        dp[0][0]=0;
        dp[0][1]=-prices[0];
        for(int i=1;i<n;++i){
            dp[i][0]=Math.max(dp[i-1][0],dp[i-1][1]+prices[i]);
            dp[i][1]=Math.max(dp[i-1][1],dp[i-1][0]-prices[i]);
        }
        return dp[n-1][0];
    }

    @Test
    public void testReverse(){
        int re=reverse(123);
        System.out.println(re);
    }

    /**
     * 7. 整数反转
     * 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
     * @param x
     * @return
     */
    public int reverse(int x){
        if(x==Integer.MIN_VALUE) return 0;
        int neg=x<0?-1:1;
        x *=neg;
        int ret=0;
        while (x>0){
            int n=ret;
            n *=10;
            n +=x % 10;
            x /=10;
            if(n/10!=ret){
                return 0;
            }
            ret=n;
        }
        return ret *neg;
    }


    /**
     * 兔子问题。
     */
    @Test
    public void calRabbit(){
        int i=0;
        for(i=1;i<=20;i++){
            System.out.println(f(i));
        }

    }

    public static int f(int x){
        if(x==1 || x==2){
            return 1;
        }
        else{
            return f(x-1)+f(x-2);
        }
    }

    /**
     * 质素
     */
    @Test
    public void testZhishu(){
        int i=0;
        for(i=2;i<=200;i++){
            if(iszhishu(i)){
                System.out.println(i);
            }
        }
    }

    public boolean iszhishu(int x){
        for(int i=2;i<x/2;i++){
            if(x % 2==0){
                return false;
            }
        }
        return true;
    }

    @Test
    public void testShuixianhua(){
        for(int i=100;i<=999;i++){
            if(shuixianhua(i)){
                System.out.println("结果："+i);
            }
        }
    }


    public boolean shuixianhua(int x){
        int i=0,j=0,k=0;
        i = x / 100;
        j = (x % 100) /10;
        k= x%10;
        System.out.println("分解：i:"+i+",j:"+j+",k:"+k);
        if(x == i*i*i+j*j*j+k*k*k)
            return true;
        else
            return false;
    }

    /**
     * 自由落体10次
     */
    @Test
    public void testEx(){
        double s=0;
        double t=100;
        for(int i=1;i<=20;i++){
            s+=t;
            t=t/2;
        }
        System.out.println("s经过总米数:"+s);
        System.out.println("t高度："+t);
    }



}
