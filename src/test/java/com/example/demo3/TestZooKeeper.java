package com.example.demo3;

import com.example.demo3.model.TreeNode;
import com.example.demo3.zk.ZooKeeperSession;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

@Slf4j
public class TestZooKeeper extends Demo3ApplicationTests {

    @Test
    public void testHamming(){
        //int a=hammingWeight(88);
        int a2=hammingWeight(999999);
        System.out.println(a2);
    }

    public int hammingWeight2(int n){
        int sum=0;
        while (n!=0){
            sum++;
            n &=(n-1);
        }
        return sum;
    }

    /**
     * 位1的个数
     * @param n
     * @return
     */
    public int hammingWeight(int n){
        int bits=0;
        int mask=1;
        for(int i=0;i<32;i++){
            if((n & mask)!=0){
                bits++;
            }
            mask<<=1;
        }
        return bits;
    }

    public List<String> findWords(char[][] board,String[] words){
        List<String> res=new ArrayList<>();
        //判断每个单词
        for(String word:words){
            if(exist(board,word)){
                res.add(word);
            }
        }
        return res;
    }

    public boolean exist(char[][] board,String word){
        int rows=board.length;
        if(rows==0){
            return false;
        }
        int cols=board[0].length;
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                if(existRecursive(board,i,j,word,0)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean existRecursive(char[][] board,int row,int col,
                                   String word,int index){
        if(row<0 || row>=board.length|| col<0||
        col>=board[0].length){
            return false;
        }
        if(board[row][col]!=word.charAt(index)){
            return false;
        }
        if(index==word.length()-1){
            return true;
        }
        char temp=board[row][col];
        board[row][col]='$';
        boolean up=existRecursive(board,row-1,col,word,index-1);
        if(up){
            board[row][col]=temp;
            return true;
        }
        boolean down=existRecursive(board,row+1,col,word,index+1);
        if (down) {
            board[row][col]=temp;
            return true;
        }
        boolean left=existRecursive(board,row,col-1,word,index-1);
        if(left){
            board[row][col]=temp;
            return true;
        }
        boolean right=existRecursive(board,row,col+1,word,index+1);
        if(right){
            board[row][col]=temp;
            return true;
        }
        board[row][col]=temp;
        return false;
    }



    @Test
    public void testSqrt(){
        //double x=mySqrt(15);
        //int x=mySqrt2(15);
        int y=mySqrt2(101);
        System.out.println(y);
    }

    public int mySqrt3(int x){
        if(x==0){
            return 0;
        }
        double C=x,x0=x;
        while (true){
            double xi=0.5*(x0+C/x0);
            if(Math.abs(x0-xi)<1e-7){
                break;
            }
            x0=xi;
        }
        return (int)x0;
    }

    public int mySqrt2(int x){
        int l=0,r=x,ans=-1;
        while (l<r){
            int mid=l+(r-l)/2;
            if((long)mid*mid<=x){
                ans=mid;
                l=mid+1;
            }
            else{
                r=mid-l;
            }
        }
        return ans;
    }

    /**
     * x 的平方根
     * @param x
     * @return
     */
    public double mySqrt(double x){
        if(x==0){
            return 0;
        }
        double ans=(double)Math.exp(0.5*Math.log(x));
        return (double)(ans+1)*(ans+1)<=x?ans+1:ans;
    }

    public int maxDepth2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        int ans = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size > 0) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
                size--;
            }
        }
        return ans;
    }


    /**
     * 二叉树的最大深度
     * @param root
     * @return
     */
    public int maxDepth(TreeNode root){
        if(root == null){
            return 0;
        }
        else{
            int leftHeight=maxDepth(root.left);
            int rightHeight=maxDepth(root.right);
            return Math.max(leftHeight,rightHeight)+1;
        }
    }


    /**
     * 测试分布式锁。
     */
    @Test
    public void testZookeeper() {
        Long orderId = 1L;
        ZooKeeperSession zooKeeperSession = new ZooKeeperSession();
        log.info("获取锁");
        zooKeeperSession.acquireDistributeLock(orderId);
        log.info("执行业务逻辑...");
        zooKeeperSession.releaseDistributeLock(orderId);
        log.info("释放锁");
    }

}
