package com.example.demo3;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class TestView extends Demo3ApplicationTests {

    @Test
    public void testA(){
        String a="";
        String[] b=a.split(",");
        log.info("返回："+b.toString());

//        Integer a=10;
//        method(a);
//        System.out.println(a);
    }



    public void method(Integer a){
        for(Integer i=10;i>0;i--){
            a++;
        }
        System.out.println(a);
    }


    class C{
        public C() {
            System.out.println("C构造方法");
            this.print();
        }
        void print() {
            System.out.println("这是C中的this调用");
        }
    }

    class B extends C{
        public B() {
            System.out.println("B构造方法");
            this.print();
        }
        void print() {
            System.out.println("这是B中的this调用");
        }
    }
    class A extends B{
        public A() {
            System.out.println("A构造方法");
            this.print();
        }
        void print(){
            System.out.println("这是A中的this调用");
        }
    }


    @Test
    public void Main() {
            A a = new A();
            System.out.println("=====================");
            B b = new A();
            System.out.println("=====================");
            C c = new A();
            System.out.println("=====================");
    }


}
