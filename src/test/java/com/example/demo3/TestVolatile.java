package com.example.demo3;

import com.example.demo3.common.ThreadVolatile;
import com.example.demo3.model.TreeNode;
import com.example.demo3.zk.KthLargest;
import com.example.demo3.zk.Solution;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestVolatile extends Demo3ApplicationTests {

    public List<List<Integer>> levelOrder2(TreeNode root){
        if(root==null){
            return new ArrayList<List<Integer>>();
        }
        //用来存放最终结果
        List<List<Integer>> res=new ArrayList<List<Integer>>();
        dfs(1,root,res);
        return res;
    }

    /**
     * 迭代+递归 多图演示 102.二叉树的层次遍历
     * @param index
     * @param root
     * @param res
     */
    private void dfs(int index,TreeNode root,List<List<Integer>> res){
        //假设res是[ [1],[2,3] ]， index是3，就再插入一个空list放到res中
        if(res.size()<index){
            res.add(new ArrayList<Integer>());
        }
        //将当前节点的值加入到res中，index代表当前层，假设index是3，节点值是99
        //res是[ [1],[2,3] [4] ]，加入后res就变为 [ [1],[2,3] [4,99] ]
        res.get(index-1).add(root.val);
        //递归的处理左子树，右子树，同时将层数index+1
        if(root.left!=null){
            dfs(index+1,root.left,res);
        }
        if(root.right!=null){
            dfs(index+1,root.right,res);
        }
    }

    /**
     * 迭代+递归 多图演示 102.二叉树的层次遍历
     * @param root
     * @return
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null) {
            return new ArrayList<List<Integer>>();
        }
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        //将根节点放入队列中，然后不断遍历队列
        queue.add(root);
        while (queue.size() > 0) {
            //获取当前队列的长度，这个长度相当于 当前这一层的节点个数
            int size = queue.size();
            ArrayList<Integer> tmp = new ArrayList<Integer>();
            //将队列中的元素都拿出来(也就是获取这一层的节点)，放到临时list中
            //如果节点的左/右子树不为空，也放入队列中
            for (int i = 0; i < size; ++i) {
                TreeNode t = queue.remove();
                tmp.add(t.val);
                if (t.left != null) {
                    queue.add(t.left);
                }
                if (t.right != null) {
                    queue.add(t.right);
                }
            }
            //将临时list加入最终返回结果中
            res.add(tmp);
        }
        return res;
    }


    /**
     * 简单的一次遍历
     * 该解决方案遵循 方法二 的本身使用的逻辑，但有一些轻微的变化。在这种情况下，我们可以简单地继续在斜坡上爬升并持续增加从连续交易中获得的利润，
     * 而不是在谷之后寻找每个峰值。最后，我们将有效地使用峰值和谷值，但我们不需要跟踪峰值和谷值对应的成本以及最大利润，
     * 但我们可以直接继续增加加数组的连续数字之间的差值，如果第二个数字大于第一个数字，我们获得的总和将是最大利润。这种方法将简化解决方案。
     *
     * @param prices
     * @return
     */
    public int maxProfit3(int[] prices) {
        int maxprofit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1]) {
                maxprofit += prices[i] - prices[i - 1];
            }
        }
        return maxprofit;
    }

    /**
     * 峰谷法
     * @param prices
     * @return
     */
    public int maxProfit2(int[] prices){
        int i=0;
        int valley=prices[0];
        int peak=prices[0];
        int maxprofit=0;
        while (i<prices.length-1){
            while (i<prices.length-1 && prices[i]>=prices[i+1]){
                i++;
            }
            valley=prices[i];
            while (i<prices.length-1 && prices[i]<=prices[i+1]){
                i++;
            }
            peak=prices[i];
            maxprofit+=peak-valley;
        }
        return maxprofit;
    }

    @Test
    public void testMaxProfit(){
        int[] prices=new int[]{7, 1, 5, 3, 6, 4};
        //int max= calculate(prices,0);
        //int max=maxProfit2(prices);
        int max=maxProfit3(prices);
        System.out.println(max);
    }

    /**
     * 买卖股票的最佳时机 II
     * 力扣 (LeetCode)
     * @param prices
     * @param s
     * @return
     */
    public int calculate(int prices[],int s){
        if(s>=prices.length){
            return 0;
        }
        int max=0;
        for(int start=s;start<prices.length;start++){
            int maxprofit=0;
            for(int i=start+1;i<prices.length;i++){
                if(prices[start]<prices[i]){
                    int profit=calculate(prices,i+1)+prices[i]-prices[start];
                    if(profit>maxprofit){
                        maxprofit=profit;
                    }
                }
            }
            if(maxprofit>max){
                max=maxprofit;
            }
        }
        return max;
    }

    @Test
    public void testMajority(){
        int[] a=new int[]{1,2,3,4,5,6,6,6,6,6,6};
        //int result=majorityElement(a);
        int result=majorityElement2(a);
        System.out.println(result);
    }


    /**
     * 多数元素
     * @param nums
     * @return
     */
    private Map<Integer,Integer> countNums(int[] nums){
        Map<Integer,Integer> counts=new HashMap<Integer,Integer>();
        for(int num:nums){
            if(!counts.containsKey(num)){
                counts.put(num,1);
            }
            else{
                counts.put(num,counts.get(num)+1);
            }
        }
        return counts;
    }

    public int majorityElement2(int[] nums){
        Map<Integer,Long> map=Arrays.stream(nums).boxed()
                .collect(Collectors.groupingBy(Function
                        .identity(),Collectors.counting()));
        long limit=nums.length >> 1;
        for(Map.Entry<Integer,Long> entry:map.entrySet()){
            if(entry.getValue()>limit){
                return entry.getKey();
            }
        }
        return -1;
    }

    public int majorityElement(int[] nums){
        Map<Integer,Integer> counts=countNums(nums);
        Map.Entry<Integer,Integer> majorityEntry=null;
        for(Map.Entry<Integer,Integer> entry:counts.entrySet()){
            if(majorityEntry==null || entry.getValue()>majorityEntry.getValue()){
                majorityEntry=entry;
            }
        }
        return majorityEntry.getKey();
    }

    @Test
    public void testPow(){
        double x=myPow2(2,10);
        System.out.println(x);
    }

    public double quickMul2(double x,Long N){
        double ans=1.0;
        // 贡献的初始值为 x
        double x_contribute=x;
        // 在对 N 进行二进制拆分的同时计算答案
        while (N>0){
            if(N%2==1){
                // 如果 N 二进制表示的最低位为 1，那么需要计入贡献
                ans *=x_contribute;
            }
            // 将贡献不断地平方
            x_contribute *=x_contribute;
            // 舍弃 N 二进制表示的最低位，这样我们每次只要判断最低位即可
            N /=2;
        }
        return ans;
    }

    /**
     * Pow(x, n)
     */
    public double quickMul(double x,long N){
        if(N==0){
            return 1.0;
        }
        double y= quickMul(x,N/2);
        return N % 2 ==0?y*y:y*y*x;
    }

    public double myPow(double x,int n){
        Long N=Long.valueOf(n);
        return N>=0?quickMul(x,N):1.0/quickMul(x,-N);
    }

    public double myPow2(double x,int n){
        Long N=Long.valueOf(n);
        return N>=0?quickMul2(x,N):1.0/quickMul(x,-N);
    }


//    /**
//     * 验证二叉搜索树
//     * @param root
//     * @return
//     */
//    @Test
//    public boolean isValidBST(TreeNode root) {
//        Stack<TreeNode> stack = new Stack();
//        double inorder = - Double.MAX_VALUE;
//
//        while (!stack.isEmpty() || root != null) {
//            while (root != null) {
//                stack.push(root);
//                root = root.left;
//            }
//            root = stack.pop();
//            // 如果中序遍历得到的节点的值小于等于前一个 inorder，说明不是二叉搜索树
//            if (root.val <= inorder) return false;
//            inorder = root.val;
//            root = root.right;
//        }
//        return true;
//    }

    @Test
    public void testSlidingWindow(){
        int[] nums=new int[]{1,3,-1,-3,5,3,6,7};
        //int[] re= maxSlidingWindow(nums,3);
        Solution solution=new Solution();
        int[] re= solution.maxSlidingWindow2(nums,3);
        System.out.println(Arrays.toString(re));
    }

    /**
     * 滑动窗口求最大值。
     * @param nums
     * @param k
     * @return
     */
    public int[] maxSlidingWindow(int[] nums,int k){
        int n =nums.length;
        if(n==0){
            return nums;
        }
        int result[] =new int[n-k+1];
        for(int i=0;i<result.length;i++){
            int max=Integer.MIN_VALUE;
            for(int j=0;j<k;j++){
                max=Math.max(max,nums[i+j]);
            }
            result[i]=max;
        }
        return result;
    }




}
