package com.example.demo3;

import com.example.demo3.leetcode.ParkingSystem;
import com.example.demo3.zk.ListNode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.storm.shade.org.eclipse.jetty.util.ajax.JSON;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;

public class Test1219 extends Demo3ApplicationTests {

    /**
     * 1652. 拆炸弹
     * @param code
     * @param k
     * @return
     */
    public int[] decrypt(int[] code, int k) {
        //新数组，用来存放答案
        int[] newCode = new int[code.length];
        if (k == 0) {
            return newCode;
        }
        //偏移量，当K是负数的时候，要等价代换一下
        int offset = k > 0 ? 0 : code.length + k - 1;
        //对K取绝对值
        k = k > 0 ? k : -k;
        for (int i = 0 + offset; i < code.length + offset; i++) {
            int sum = 0;
            for (int j = 0; j < k; j++) {
                //通过取余就可以实现循环操作
                sum += code[(i + 1 + j) % code.length];
            }
            //对0-n-1下标的新数组赋值
            newCode[i - offset] = sum;
        }
        return newCode;
    }

    @Test
    public void testMajorityElement() {
        //int[] mat = new int[]{1};
        int[] mat = new int[]{1, 2, 3, 2, 2, 2, 5, 4, 2};
        int out = majorityElement(mat);
        System.out.println(JSON.toString(out));
    }

    @Test
    public void testMaxPower(){
        String str="abbcccddddeeeeedcba";
        System.out.println(maxPower(str));
    }

    /**
     * 剑指 Offer 39. 数组中出现次数超过一半的数字
     * @param nums
     * @return
     */
    public int majorityElement(int[] nums) {
        int count=nums.length/2;
        HashMap<Integer,Integer> map= new HashMap<Integer, Integer>();
        for(int i=0;i<nums.length;i++){
            if(!map.containsKey(nums[i])){
                map.put(nums[i],1);
            }
            else{
                int curCount=map.get(nums[i])+1;
                map.put(nums[i],curCount);
            }
            if(map.get(nums[i])>count){
                return nums[i];
            }
        }
        return nums[0];
    }

    @Test
    public void testMinimumAbsDifference() {
        int[] mat = new int[]{4, 2, 1, 3};
        List<List<Integer>> out = minimumAbsDifference(mat);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1200. 最小绝对差
     * @param arr
     * @return
     */
    public List<List<Integer>> minimumAbsDifference(int[] arr) {
        List<List<Integer>> list = Lists.newArrayList();
        Arrays.sort(arr);
        Integer temp = Integer.MAX_VALUE;
        for (int i = 1; i < arr.length; i++) {
            Integer one=arr[i] - arr[i - 1];
            if (one < temp) {
                temp = one;
            }
        }
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] - arr[i - 1] == temp) {
                List<Integer> one = Lists.newArrayList();
                one.add(arr[i]);
                one.add(arr[i-1]);
                list.add(one);
            }
        }
        return list;
    }

    @Test
    public void testKWeakestRows(){
        String allowed="cad";
        int[][] mat=new int[][]{{1,1,0,0,0},
                {1,1,1,1,0},
                {1,0,0,0,0},
                {1,1,0,0,0},
                {1,1,1,1,1}
        };
        int[] out=kWeakestRows(mat,3);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1337. 矩阵中战斗力最弱的 K 行
     * @param mat
     * @param k
     * @return
     */
    public int[] kWeakestRows(int[][] mat, int k) {
        int[] a=new int[mat.length];
        int[] result=new int[k];
        for(int i=0;i<mat.length;i++){
            a[i]=count(mat[i])*100+i;
        }
        Arrays.sort(a);
        for(int i=0;i<a.length;i++){
            result[i]=a[i]%100;
        }
        return result;
    }

    public int count(int[] nums){
        int result=0;
        for(int i:nums){
           if(i!=1){
               break;
           }
           result++;
       }
        return result;
    }


    /**
     * 面试题 02.03. 删除中间节点
     * @param node
     */
    public void deleteNode2(ListNode node){
        node.val=node.next.val;
        node.next=node.next.next;
    }

    @Test
    public void testCountConsistentStrings(){
//        String allowed="abc";
//        String[] words=new String[]{"a","b","c","ab","ac","bc","abc"};
//        String allowed="ab";
//        String[] words=new String[]{"ad","bd","aaab","baa","badab"};
        String allowed="cad";
        String[] words=new String[]{"cc","acd","b","ba","bac","bad","ac","d"};
        int out=countConsistentStrings(allowed,words);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1684. 统计一致字符串的数目
     * @param allowed
     * @param words
     * @return
     */
    public int countConsistentStrings(String allowed, String[] words) {
        if (null==allowed) {
            return 0;
        }
        int re = 0;
        for (int i = 0; i < words.length; i++) {
            String oneWord=words[i];
            if (null!=oneWord) {
                char[] ch=oneWord.toCharArray();
                Boolean flag=true;
                for(int j=0;j<ch.length;j++){
                    if(!allowed.contains(String.valueOf(ch[j]))){
                        flag=false;
                    }
                }
                if(flag){
                    re++;
                }
            }
        }
        return re;
    }


    /**
     * 1266. 访问所有点的最小时间
     * 后一个横坐标减去前一个横坐标 后一个纵坐标减去前一个纵坐标
     * 比较两个的绝对值谁大,然后sum加上大的绝对值,
     * 遍历到末尾返回即可。
     * @param points
     * @return
     */
    public int minTimeToVisitAllPoints(int[][] points) {
        int sum=0;
        for(int i=0,j=i+1;j<points.length;i++,j++){
            int x=Math.abs(points[j][0]-points[i][0]);
            int y=Math.abs(points[j][1]-points[i][1]);
            sum+=y>=x?y:x;
        }
        return sum;
    }


    @Test
    public void testDiagonalSum(){
        //int[][] nums={{1,2,3},{4,5,6},{7,8,9}};
        int[][] nums={{5}};
        int out=diagonalSum(nums);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1572. 矩阵对角线元素的和
     * @param mat
     * @return
     */
    public int diagonalSum2(int[][] mat) {
        int n=mat.length;
        int sum=0;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(i==j||i+j==n-1){
                    sum+=mat[i][j];
                }
            }
        }
        return sum;
    }

    /**
     * 1572. 矩阵对角线元素的和
     * @param mat
     * @return
     */
    public int diagonalSum(int[][] mat) {
        int sum=0;
        HashMap<String, Integer> map= new HashMap<String, Integer>();
        int len= mat.length;
        //左对角线
        for(int i=0;i<len;i++){
            map.put(i+"-"+i,1);
        }
        //右对角线
        for(int j=0;j<len;j++){
             map.put(j+"-"+(len-1-j),1);
        }
        for (String key : map.keySet()) {
            sum+=mat[Integer.parseInt(key.split("-")[0])][Integer.parseInt(key.split("-")[1])];
        }
        return sum;
    }


    @Test
    public void testFindNumbers(){
        int[] nums=new int[]{12,345,2,6,7896};
        int out=findNumbers(nums);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1295. 统计位数为偶数的数字
     * @param nums
     * @return
     */
    public int findNumbers(int[] nums) {
        int count=0;
        for(int i=0;i<nums.length;i++){
            if(String.valueOf(nums[i]).length()%2==0 ){
                count++;
            }
        }
        return count;
    }


    /**
     * 面试题 02.03. 删除中间节点
     * @param node
     */
    public void deleteNode(ListNode node) {
        node.val=node.next.val;
        node.next=node.next.next;
    }

    @Test
    public void testCalculate(){
        String a="AB";
        int out=calculate(a);
        System.out.println(JSON.toString(out));
    }

    /**
     * LCP 17. 速算机器人
     * @param s
     * @return
     */
    public int calculate(String s) {
        int x=1;
        int y=0;
        for(char c:s.toCharArray()){
            if(c=='A'){
                x=2*x+y;
            }
            else{
                y=2*y+x;
            }
        }
        return x+y;
    }

    //627. 变更性别
    //update salary set sex=case sex when 'f' then 'm' else 'f' end;

    /**
     * 605. 种花问题
     * @param flowerbed
     * @param n
     * @return
     */
    public boolean canPlaceFlowers2(int[] flowerbed, int n) {
        int count=0;
        for(int i=0;i<flowerbed.length;){
            if(flowerbed[i]==1){
                i+=2;
            }
            else if(i==flowerbed.length-1||flowerbed[i+1]==0){
                count++;
                i+=2;
            }
            else{
                i+=3;
            }
        }
        return count>=n;
    }

    /**
     * 605. 种花问题
     * @param flowerbed
     * @param n
     * @return
     */
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        int count=0;
        int m=flowerbed.length;
        int prev=-1;
        for(int i=0;i<m;i++){
            if(flowerbed[i]==1){
                if(prev<0){
                    count+=i/2;
                }
                else{
                    count+=(i-prev-2)/2;
                }
                prev=i;
            }
        }
        if(prev<0){
            count+=(m+1)/2;
        }
        else{
            count+=(m-prev-1)/2;
        }
        return count>=n;
    }

    @Test
    public void testMaxDepth(){
        String a="(((((((()))";
        int out=maxDepth(a);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1614. 括号的最大嵌套深度
     * @param s
     * @return
     */
    public int maxDepth(String s) {
        int max=0;
        int num=0;
        for(int i=0;i<s.length();i++){
            char c=s.charAt(i);
            if(c=='('){
                num++;
            }
            else if(c==')'){
                num--;
            }
            if(num>max){
                max=num;
            }
        }
        return max;
    }

    /**
     * 205. 同构字符串
     * 给定两个字符串 s 和 t，判断它们是否是同构的。
     * 如果 s 中的字符可以被替换得到 t ，那么这两个字符串是同构的。
     * 所有出现的字符都必须用另一个字符替换，同时保留字符的顺序。两个字符不能映射到同一个字符上，但字符可以映射自己本身。
     * @param s
     * @param t
     * @return
     */
    public boolean isIsomorphic(String s, String t) {
        Map<Character,Character> s2t=new HashMap<Character, Character>();
        Map<Character, Character> t2s=new HashMap<Character,Character>();
        int len=s.length();
        for(int i=0;i<len;i++){
            char x=s.charAt(i);
            char y=t.charAt(i);
            if((s2t.containsKey(x) && s2t.get(x)!=y) || (t2s.containsKey(y) && t2s.get(y)!=x)){
                return false;
            }
            s2t.put(x,y);
            t2s.put(y,x);
        }
        return true;
    }

    @Test
    public void testGame(){
        int[] input=new int[]{1,2,3};
        int[] input2=new int[]{3,2,1};
        int out=game(input,input2);
        System.out.println(JSON.toString(out));
    }

    /**
     * LCP 01. 猜数字
     * 小A 和 小B 在玩猜数字。小B 每次从 1, 2, 3 中随机选择一个，小A 每次也从 1, 2, 3 中选择一个猜。他们一共进行三次这个游戏，请返回 小A 猜对了几次？
     * @param guess
     * @param answer
     * @return
     */
    public int game(int[] guess, int[] answer) {
        int re=0;
        for(int i=0;i<guess.length;i++){
            if(guess[i]==answer[i]){
                re++;
            }
        }
        return re;
    }

    @Test
    public void testKidsWithCandies(){
        int[] input=new int[]{4,2,1,1,2};
        List<Boolean> out=kidsWithCandies(input,1);
        System.out.println(JSON.toString(out));
    }

    /**
     * 1431. 拥有最多糖果的孩子
     * @param candies
     * @param extraCandies
     * @return
     */
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        int a = Arrays.stream(candies).max().getAsInt();
        List<Boolean> list =new ArrayList<>();
        for (int i = 0; i < candies.length; i++) {
            if (candies[i] + extraCandies >= a) {
                list.add(true);
            } else {
                list.add(false);
            }
        }
        return list;
    }

    @Test
    public void testDefangIPaddr(){
        String input="1.1.1.1";
        String out=defangIPaddr(input);
        System.out.println(out);
    }

    /**
     * 1108. IP 地址无效化
     * @param address
     * @return
     */
    public String defangIPaddr(String address) {
        return  address.replace(".","[.]");
    }

    @Test
    public void testShuffle(){
        int[] input = new int[]{2,5,1,3,4,7};
        int[] re=shuffle(input,3);
        System.out.println(Arrays.toString(re));
    }

    /**
     * 1470. 重新排列数组
     * @param nums
     * @param n
     * @return
     */
    public int[] shuffle(int[] nums, int n) {
        if(null==nums || nums.length!=n*2){
            return null;
        }
        int[] re=new int[n*2];
        for(int i=0;i<n;i++){
            re[2*i]=nums[i];
            re[2*i+1]=nums[n+i];
        }
        return re;
    }

    @Test
    public void test(){
         ParkingSystem obj = new ParkingSystem(1, 1, 0);
        boolean out = obj.addCar(1);
        System.out.println(out);
        out = obj.addCar(2);
        System.out.println(out);
        out = obj.addCar(3);
        System.out.println(out);
    }

    @Test
    public void testMaximumWealth(){
        //int[][] accounts = new int[][]{{1,2,3},{3,2,1}};
        int[][] accounts = new int[][]{{1,5},{7,3},{3,5}};
        int re=maximumWealth(accounts);
        System.out.println(re);
    }

    /**
     * 1672. 最富有客户的资产总量
     * @param accounts
     * @return
     */
    public int maximumWealth(int[][] accounts) {
        int max=0;
        for(int i=0;i<accounts.length;i++){
            int maxOne=0;
            for(int j=0;j<accounts[i].length;j++){
                maxOne+=accounts[i][j];
            }
            if(max<maxOne){
                max=maxOne;
            }
        }
        return max;
    }

    @Test
    public void testRunningSum(){
        int[] nums=new int[]{3,1,2,10,1};
        int[] re=runningSum2(nums);
        System.out.println(Arrays.toString(re));
    }

    /**
     * 1480. 一维数组的动态和
     * @param nums
     * @return
     */
    public int[] runningSum2(int[] nums) {
        for(int i=1;i<nums.length;i++){
            nums[i]=nums[i]+nums[i-1];
        }
        return nums;
    }

    /**
     * 1480. 一维数组的动态和
     * @param nums
     * @return
     */
    public int[] runningSum(int[] nums) {
        int sum=0;
        int[] result=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            sum+=nums[i];
            result[i]=sum;
        }
        return result;
    }

    /**
     * 1446. 连续字符
     * @param s
     * @return
     */
    public int maxPower(String s) {
        int ans=1,cnt=1;
        for(int i=1;i<s.length();++i){
            if(s.charAt(i)==s.charAt(i-1)){
                ++cnt;
                ans=Math.max(ans,cnt);
            }
            else{
                cnt=1;
            }
        }
        return ans;
    }



}
