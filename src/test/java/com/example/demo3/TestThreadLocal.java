package com.example.demo3;

import com.example.demo3.common.ThreadLocalDemo;
import org.junit.Test;
import sun.awt.geom.AreaOp;

public class TestThreadLocal extends Demo3ApplicationTests {
    /*定义一个全局变量 来存放线程需要的变量*/
    public static ThreadLocal<Integer> ti = new ThreadLocal<Integer>();

    @Test
    public void testThread() {
        /*创建两个线程*/
        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Double cur = Math.random();
                    System.out.println("cur:" + cur);
                    Double d = cur * 10;
                    /*存入当前线程独有的值*/
                    ti.set(d.intValue());
                    new A().get();
                    new B().get();
                }
            }).start();
        }
    }

    static class A {
        public void get() {
            /*取得当前线程所需要的值*/
            System.out.println(ti.get());
        }
    }

    static class B {
        public void get() {
            /*取得当前线程所需要的值*/
            System.out.println(ti.get());
        }
    }

    @Test
    public void testThread2() {
        for (int i = 0; i < 1; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Double d = Math.random() * 10;
                    ThreadLocalDemo.getThreadInstance().setName("name" + d);
                    new A2().get();
                    new B2().get();
                }
            }).start();
        }
    }

    static class A2 {
        public void get() {
            System.out.println(ThreadLocalDemo.getThreadInstance().getName());
        }
    }

    static class B2 {
        public void get() {
            System.out.println(ThreadLocalDemo.getThreadInstance().getName());
        }
    }

}
