package com.example.demo3;

import com.alibaba.fastjson.JSON;
import com.example.demo3.cache.UserCache;
import com.example.demo3.model.UserInfo;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
public class EhcacheTest extends Demo3ApplicationTests {

    /**
     * 11. 盛最多水的容器
     * @param height
     * @return
     */
    public int maxArea(int[] height) {
        int l=0,r=height.length-1;
        int ans=0;
        while (l<r){
            int area=Math.min(height[l],height[r]*(r-1));
            ans=Math.max(ans,area);
            if(height[l]<=height[r]){
                ++l;
            }
            else{
                --r;
            }
        }
        return ans;
    }


    @Test
    public void testTwoTotal(){
        int a[]=new int[]{2,1,3,4,5,6, 7, 11, 15};
        int[] result=twoTotal2(a,9);
        System.out.println(Arrays.toString(result));
    }

    /**
     * 2数之和
     * @param inputs
     * @param target
     */
    public int[] twoTotal2(int[] inputs,int target){
        HashMap map= Maps.newHashMap();
        for(int i=0;i<inputs.length;i++){
            map.put(inputs[i],i);
        }
        for(int j=0;j<inputs.length;j++){
            int complement=target-inputs[j];
            if(map.containsKey(complement) && !map.get(complement).equals(j)){
                return new int[]{j,Integer.parseInt(map.get(complement).toString())};
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    /**
     * 2数之和
     * @param inputs
     * @param target
     */
    public int[] twoTotal(int[] inputs,int target){
        for(int i=0;i<inputs.length;++i){
            for(int j=i+1;j<inputs.length;++j){
                if(inputs[i]==target-inputs[j]){
                    return new int[]{i,j};
                }
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    @Autowired
    private UserCache userCache;

    @Resource
    private CacheManager cacheManager;

    @Test
    public void getCacheInfo(){
        // 显示所有的Cache空间
        System.out.println(StringUtils.join(cacheManager.getCacheNames(), ","));
        Cache cache = cacheManager.getCache("userCache");
        cache.put("key", "123");
        System.out.println("缓存成功");
        String res = cache.get("key", String.class);
        System.out.println(res);

    }

    @Test
    public void testStream(){
//        // 计算空字符串
//        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
//        // 并行处理
//        Long count = strings.parallelStream().filter(string -> string.isEmpty()).count();
//        System.out.println("空字符串的数量为: " + count);
//
//        count = strings.stream().filter(string->string.isEmpty()).count();
//        System.out.println("空字符串数量为: " + count);

        List<Integer> source = buildIntRange();

        // 传统方式的遍历
        long start = System.currentTimeMillis();
        for (int i = 0; i < source.size(); i++) {
            String a="test";
//            try {
//                TimeUnit.MILLISECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
        //System.out.println("传统方式 : " + (System.currentTimeMillis() - start) + "ms");
        log.info("传统方式 : " + (System.currentTimeMillis() - start) + "ms");

        // 单管道stream
        start = System.currentTimeMillis();
        source.stream().forEach(r -> {
            String a="test";
//            try {
//                TimeUnit.MILLISECONDS.sleep(1);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        });
        log.info("stream : " + (System.currentTimeMillis() - start) + "ms");

        // 多管道parallelStream
        start = System.currentTimeMillis();
        source.parallelStream().forEach(r -> {
            String a="test";
//            try {
//                TimeUnit.MILLISECONDS.sleep(1);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        });
        log.info("parallelStream : " + (System.currentTimeMillis() - start) + "ms");

    }

    public static List<Integer> buildIntRange() {
        List<Integer> numbers = new ArrayList<>(5);
        for (int i = 0; i < 60000; i++)
            numbers.add(i);
        return Collections.unmodifiableList(numbers);
    }


}
